import 'package:flutter/material.dart';
import 'workspace/guess_game.dart';
import 'package:deebug_classes/workspace/counter.dart';

void main(){
  runApp(GuessApp());
}

class GuessApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Guess Game',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.green),
      home: DeebugCounter(),
      // home: GuessHome(),
    );
  }
}
