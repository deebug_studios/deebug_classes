
  ///ASSIGNMENT:
  /// ADD NUMBER OF ATTEMPTS BEFORE A CORRECT GUESS
  /// USE SNACKBAR INSTEAD ON LINE 80
  /// 
  /// APPS TO DOWNLOAD (from playstore): Flutter Gallery, Flutter UI Challenge
  /// Flutter Catalog
  /// Flutter Tutorial
  /// Learn Dart

import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class GuessHome extends StatefulWidget{
  @override
  GuessHomeState createState(){
    return GuessHomeState();
  }
}

class GuessHomeState extends State<GuessHome>{

  int ?rand;
  bool guessGotten = false;
  int noOfAttempts = 0;

  TextEditingController _guessController = TextEditingController();

  @override
  initState(){
    super.initState();
    rand = Random().nextInt(10);
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: _guessController,
              keyboardType: TextInputType.number,
              maxLength: 2,
              selectionControls: CupertinoTextSelectionControls(),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          guessGotten ? ElevatedButton.icon(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
            ),
            label: Text('Play Again'),
            icon: Icon(Icons.replay),
            onPressed: (){
              setState(() {
                rand = Random().nextInt(10);
                guessGotten = false;
              });
            },
          ) : ElevatedButton(
            child: Text('Guess'),
            onPressed: (){
              verifyGuess();
            },
          )
        ],
      )
    );
  }

  verifyGuess(){
    noOfAttempts++;

    if(rand! > int.parse(_guessController.text)){
      Fluttertoast.showToast(
        msg: 'Guess too low',
        toastLength: Toast.LENGTH_SHORT,
      );
      _guessController.clear();
    }
    else if(rand! < int.parse(_guessController.text)){
      Fluttertoast.showToast(
        msg: 'Guess too high',
        toastLength: Toast.LENGTH_SHORT,
      );
      _guessController.clear();
    }else{
      Fluttertoast.showToast(
        msg: 'Correct guess\n Attempted: $noOfAttempts times',
        toastLength: Toast.LENGTH_SHORT,
      ).then((value){
        _guessController.clear();
        setState(() {
          noOfAttempts = 0;
          guessGotten = true;
        });
      });
    }
  }
}