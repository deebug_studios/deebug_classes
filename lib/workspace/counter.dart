import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class DeebugCounter extends StatefulWidget{
  @override
  DeebugCounterState createState(){
    return DeebugCounterState();
  }
}

class DeebugCounterState extends State<DeebugCounter>{

  int counter = 0;
  void setCounter(int count)async{
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt('box', count);
  }

  void getCounter()async{
    final pref = await SharedPreferences.getInstance();
    int ?temp_counter = pref.getInt('box');
    
    setState((){
      counter = temp_counter!;
    });
  }


  @override
  initState(){
    super.initState();
    getCounter();
  }


  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('Counter'),
        centerTitle: true,
      ),
      floatingActionButton: Padding(
        padding: EdgeInsets.fromLTRB(40.0, 0.0, 10.0, 0.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FloatingActionButton(
              child: Icon(Icons.exposure_minus_1_outlined),
              onPressed: (){
                setState((){
                  counter--;
                });
                setCounter(counter);
              },
            ),
            
            FloatingActionButton(
              child: Icon(Icons.exposure_plus_1),
              onPressed: ()async{
                setState((){
                  counter++;
                });
                setCounter(counter);
              },
            ),
          ],
        ),
      ),
      
      
      body: Center(
        child: Text(
          '$counter',
          style: TextStyle(fontSize: 60, color: Colors.red)
        ),
      )
    );
  }
}
